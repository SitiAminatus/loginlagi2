public class NasiKuning extends Nasi {
	NasiKuning nasi;

	public NasiKuning(NasiKuning nasi) {
		this.nasi= nasi;
	}

	public String getDescription() {
		return nasi.getDescription() + ", Nasi";
	}

	public double cost() {
		return .10 + nasi.cost();
	}
}
